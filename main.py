from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QFileDialog

from matplotlib import pyplot as plt
import matplotlib as mpl
from window import Ui_MainWindow
from classes.BgWorker import WorkerClass
from sqlalchemy import create_engine
import sys
import os
from scipy import stats
import datetime
from sklearn.preprocessing import LabelEncoder
import numpy as np
import math
import pandas as pd
import pickle
from functools import partial


def catch_exceptions(t, val, tb):
    QtWidgets.QMessageBox.critical(None, "An exception was raised", "Exception type: {}".format(t))
    old_hook(t, val, tb)

# Handles exceptions for debugging
old_hook = sys.excepthook
sys.excepthook = catch_exceptions


class Main(QtWidgets.QMainWindow):

    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)

        # Set up signals
        self.set_up_signals()

        # Gets the current working directory
        self._cwd = os.getcwd()

        # Get the current date and time
        self._dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")

        # Setting up the background thread and background worker
        self._thread = QThread()
        self._worker = None

        # Import Directories
        self._pickleDir = None
        self._textDir = None

        # Main data containers
        self._dataset = None

    def set_up_signals(self):
        """
        Method for setting up all the widget signals when the form loads
        """

        def update_import_type(importType):

            if importType == 'MySQL':
                self._ui.groupBoxImportSql.setEnabled(True)
                self._ui.groupBoxImportPickle.setDisabled(True)
            elif importType == 'Pickle':
                self._ui.groupBoxImportSql.setDisabled(True)
                self._ui.groupBoxImportPickle.setEnabled(True)

        # IMPORT
        self._ui.pushButtonImportSql.clicked.connect(partial(self.get_import_data, 'mysql'))
        self._ui.pushButtonImportPickle.clicked.connect(partial(self.get_import_data, 'pickle'))
        self._ui.pushButtonSelectImpDir.clicked.connect(self.get_pickle_import_dir)
        self._ui.comboBoxImportType.currentTextChanged.connect(update_import_type)

        # PLOT
        self._ui.pushButtonPlot.clicked.connect(self.get_plot_data)
        self._ui.pushButtonHist.clicked.connect(self.get_plot_hist)

    # IMPORT
    @pyqtSlot()
    def get_pickle_import_dir(self):
        """
        Button triggered method for selecting the pickle export directory
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setNameFilter("Data File (*.pickle)")

        if dlg.exec_():
            dataDir = dlg.selectedFiles()[0]

        try:
            self._ui.lineEditImpDir.setText(dataDir)
            self._ui.statusbar.showMessage('Directory Selected')
        except BaseException as exception:
            self._ui.statusbar.showMessage('Could not update directory')
            raise exception

    @pyqtSlot()
    def get_import_data(self, dataType):
        """
        Button triggered method for importing data from the database
        :return:
        """

        if dataType == 'mysql':
            if self._ui.lineEditTableName.text() == '' or self._ui.lineEditSchemaName.text() == '':
                return
            self._worker = WorkerClass(self.import_sql_worker, self._ui.lineEditTableName.text(),
                                       self._ui.lineEditSchemaName.text(), self._ui.lineEditPassword.text())
        elif dataType == 'pickle':
            if self._ui.lineEditImpDir.text() == '':
                return
            self._worker = WorkerClass(self.import_pickle_worker, self._ui.lineEditImpDir.text())

        self._worker.update.connect(self.update_message)
        self._worker.finished.connect(self.thread_finished)
        self._worker.moveToThread(self._thread)
        self._thread.started.connect(self._worker.run_function)
        self._thread.start()
        self._ui.progressBarImport.setValue(0)

        self._ui.statusbar.showMessage('Importing Data...')

    def import_sql_worker(self, tableName, schemaName, password):
        """
        Worker for importing table from the mySQL database
        :param tableName: String [Name of the table to be imported from]
        :param schemaName: String [Name of the schema to be imported from]
        :param password: String [Database password]
        :return: (String, String)
        """

        # Set up the connection object
        engine = create_engine('mysql+pymysql://root:%s@localhost/%s' % (password, schemaName), pool_recycle=3600)
        dbConnection = engine.connect()
        self._worker.update.emit('Successfully connected to the MySQL database')
        self._dataset = pd.read_sql('SELECT * FROM %s.%s' % (schemaName, tableName), dbConnection, index_col=None)

        return 'import'

    def import_pickle_worker(self, dir):

        self._dataset = pd.read_pickle(dir)
        if not isinstance(self._dataset, pd.DataFrame):
            raise Exception('Error importing pickle datafile. Pickle data not DataFrame type')

        return 'import'

    # PLOT
    @pyqtSlot()
    def get_plot_data(self):
        """
        Method for plotting features against each other
        """

        def encode_column(data_loc):
            """
            Method for encoding dataset columns for speed
            :param data_loc: pd.DataFrame
            :return data_loc: pd.DataFrame
            """
            if isinstance(data_loc.iloc[0], str):
                le = LabelEncoder()
                le.fit(data_loc.unique())
                data_loc = le.transform(data_loc)
                self._encoded = True
            return data_loc

        # Get the appropriate widget information
        plotFeat1 = self._ui.comboBoxFeat1.currentText()
        plotFeat2 = self._ui.comboBoxFeat2.currentText()
        label = self._ui.comboBoxLabelPlot.currentText()
        ax = self._ui.widgetPlot.canvas.ax

        # Prepare the axes for plotting
        ax.set_xlabel(plotFeat1)
        ax.set_ylabel(plotFeat2)
        ax.grid(which='both')
        ax.autoscale(enable=True, tight=True)
        ax.minorticks_on()

        # Prepare the data for plotting (label column included if available)
        if label == 'None':
            plotData = self._dataset[[plotFeat1, plotFeat2]]
        else:
            plotData = self._dataset[[plotFeat1, plotFeat2, label]]
            uniqueLabels = self._dataset[label].unique()
        plotData.dropna(inplace=True)
        plotData[[plotFeat1, plotFeat2]].transform(encode_column)
        if self._encoded:
            ax.annotate('Encoded', xy=(0.05, 0.95), xycoords='axes fraction')

        # Plot the scatter plot
        if label != 'None':
            for label_inst in uniqueLabels.sort_values():
                ax.scatter(plotData.loc[plotData[label] == label_inst, plotFeat1],
                           plotData.loc[plotData[label] == label_inst, plotFeat2], label=label)
        else:
            ax.scatter(plotData[plotFeat1], plotData[plotFeat2])

        # Calculated the correlation coefficients
        corrBetweenFeat = stats.spearmanr(plotData[plotFeat1], plotData[plotFeat2])[0]
        textStr = 'Corr Between Feat = %.2f' % corrBetweenFeat
        if label != 'None':
            corrFeat1 = stats.spearmanr(plotData[label], plotData[plotFeat1])[0]
            corrFeat2 = stats.spearmanr(plotData[label], plotData[plotFeat2])[0]
            textStr = '\n'.join((textStr, 'Corr Feat 1 = %.2f' % corrFeat1, 'Corr Feat 2 = %.2f' % corrFeat2))
        ax.text(0.87, 0.25, textStr, transform=ax.transAxes, fontsize=11, verticalalignment='top',
                bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
        ax.legend()
        self.ui.widgetPlot.canvas.draw()
        ax.clear()

    @pyqtSlot()
    def get_plot_hist(self):
        """
        Button triggered event for plotting feature data
        """

        def encode_column(data_loc):
            """
            Method for encoding dataset columns for speed
            :param data_loc: pd.DataFrame
            :return data_loc: pd.DataFrame
            """
            if isinstance(data_loc.iloc[0], str):
                le = LabelEncoder()
                le.fit(data_loc.unique())
                data_loc = le.transform(data_loc)
                self._encoded = True
            return data_loc

        # Get the appropriate widget information
        ax = self._ui.widgetPlotHist.canvas.ax
        fig = self._ui.widgetPlotHist.canvas.fig
        feat = self._ui.comboBoxFeatPlot.currentText()
        label = self._ui.comboBoxLabel.currentText()
        bins = self._ui.comboBoxBin.currentText()
        norm = mpl.colors.Normalize(vmin=0, vmax=3, clip=True)

        # Prepare the axes for plotting
        ax.clear()
        ax.set_xlabel('Value')
        ax.set_ylabel('Incidence')
        ax.grid(b=True, which='major')

        # Prepare the data for plotting (label column included if available)
        if bins.isdigit():
            bins = int(bins)
        if label == 'None':
            plotData = self._dataset[feat]
        else:
            plotData = self._dataset[[feat, label]]
            uniqueLabels = self._dataset[label].unique()
        plotData.dropna(inplace=True)
        plotData.transform(encode_column)
        if self._encoded:
            ax.annotate('Encoded', xy=(0.05, 0.95), xycoords='axes fraction')
        binary = False
        if label != 'None' and len(uniqueLabels) == 2:
            binary = True
            binaryGlobalOcc = sum(plotData[label] == 1) / len(plotData.index)

        # Plot the actual histogram
        try:
            (_, histBarRange, histBarPatch) = ax.hist(plotData[feat], bins=bins, log=True)
        except KeyError as exception:
            self._ui.statusbar.showMessage('Unable to plot data type: %s' % exception)
            return
        for lowerLimit, upperLimit, patch in zip(histBarRange[:-1], histBarRange[1:], histBarPatch):
            labelData = plotData.loc[(plotData[feat] >= lowerLimit) & (plotData[feat] < upperLimit), label]
            if label == 'None':
                continue
            if binary:
                binaryLocalOcc = sum(labelData == 1) / len(labelData.index)
                ratio = binaryLocalOcc / binaryGlobalOcc
            patch.set_color(mpl.cm.jet(norm(ratio)))

        # Add normalised colour bar to figure and draw
        cax = fig.add_axes([0.9, 0.2, 0.03, 0.7], label='color_bar')
        mpl.colorbar.ColorbarBase(ax=cax, cmap=mpl.cm.jet, norm=norm, orientation="vertical")
        self._ui.widgetPlotHist.canvas.draw()
        self._ui.comboBoxFeatPlot.setCurrentIndex(self._ui.comboBoxFeatPlot.currentIndex() + 1)

    # SIGNALS
    def update_message(self, message):

        self._ui.listWidgetImportDesc.addItem(message)

    def thread_finished(self, stage):
        """
        Thread Finished method called by the background thread to indicate that the dataset has been imported
        :param stage: String [Stage of processing, i.e.
        :param message:
        """

        def update_import_descriptor():

            self._ui.listWidgetImportDesc.addItem('Number of Rows: %d' % (self._dataset.shape[0]))
            self._ui.listWidgetImportDesc.addItem('Number of Columns: %d' % (self._dataset.shape[1]))

        if stage == 'import':
            self._ui.listWidgetImportDesc.addItem('Successfully imported dataset')
            update_import_descriptor()
            self._ui.statusbar.showMessage('Import Complete')
            self._ui.comboBoxFeatPlot.addItems(list(self._dataset.columns))
            self._ui.comboBoxLabel.addItems(list(self._dataset.columns))
            self._ui.comboBoxFeat1.addItems(list(self._dataset.columns))
            self._ui.comboBoxFeat2.addItems(list(self._dataset.columns))
            self._ui.comboBoxLabelPlot.addItems(list(self._dataset.columns))

        self._ui.progressBarImport.setValue(100)
        self._thread.quit()


if __name__ == "__main__":

    # Defines a new application process
    app = QtWidgets.QApplication(sys.argv)

    # Create new UI MainWindow class object and assign the window widget to it
    mainWindow = Main()
    mainWindow.show()
    sys.exit(app.exec_())


