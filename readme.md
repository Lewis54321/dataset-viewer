This is a small GUI developed for exporting datasets (currently only csv files supported) to local MySQL databases

General Requirements:

MySQL Server
Python

Python Libraries:

pandas
pymysql
pyqt (version 5.0+)
sqlalchemy
